package cookies

import (
	"context"
	"net/http"
	"net/http/httptest"
	"testing"
	"time"

	"github.com/golang-jwt/jwt"
	"github.com/stretchr/testify/assert"
	"gitlab.com/RobertArktes/auth/internal/models"
	"go.uber.org/zap"
)

func Test_ValidateCookies(t *testing.T) {
	handlerToTest := http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		user, ok := GetUser(r.Context())
		if !ok {
			w.WriteHeader(http.StatusInternalServerError)
			return
		}

		_, err := w.Write([]byte(user.Username))

		assert.Nil(t, err)
	})

	catchError := func(val string, err error) string {
		assert.Nil(t, err)
		return val
	}

	User1 := models.User{
		Username: "admin",
	}
	User2 := models.User{
		Username: "user",
	}
	User3 := models.User{
		Username: "user1",
	}
	User4 := models.User{
		Username: "user2",
	}
	User5 := models.User{
		Username: "user3",
	}

	ctx := context.Background()
	ctx1 := context.WithValue(ctx, UsernameKey, User1)
	ctx2 := context.WithValue(ctx, UsernameKey, User2)
	ctx3 := context.WithValue(ctx, UsernameKey, User3)
	ctx4 := context.WithValue(ctx, UsernameKey, User4)
	ctx5 := context.WithValue(ctx, UsernameKey, User5)

	testTable := []struct {
		name               string
		username           string
		accessCookie       *http.Cookie
		refreshCookie      *http.Cookie
		expectedStatusCode int
	}{
		{
			name:     "Fresh cookies",
			username: "admin",
			accessCookie: &http.Cookie{
				Name:     accessCookie,
				Value:    catchError(CreateToken(ctx1, accessDuration)),
				Path:     "/",
				Expires:  jwt.TimeFunc().Add(accessDuration),
				HttpOnly: true,
			},
			refreshCookie: &http.Cookie{
				Name:     refreshCookie,
				Value:    catchError(CreateToken(ctx1, refreshDuration)),
				Path:     "/",
				Expires:  jwt.TimeFunc().Add(refreshDuration),
				HttpOnly: true,
			},
			expectedStatusCode: http.StatusOK,
		},
		{
			name:     "Expired access-cookie",
			username: "user",
			accessCookie: &http.Cookie{
				Name:     accessCookie,
				Value:    catchError(CreateToken(ctx2, 0)),
				Path:     "/",
				Expires:  time.Now(),
				HttpOnly: true,
			},
			refreshCookie: &http.Cookie{
				Name:     refreshCookie,
				Value:    catchError(CreateToken(ctx2, refreshDuration)),
				Path:     "/",
				Expires:  jwt.TimeFunc().Add(refreshDuration),
				HttpOnly: true,
			},
			expectedStatusCode: http.StatusOK,
		},
		{
			name:     "Expired cookies",
			username: "",
			accessCookie: &http.Cookie{
				Name:     accessCookie,
				Value:    catchError(CreateToken(ctx3, 0)),
				Path:     "/",
				Expires:  time.Now(),
				HttpOnly: true,
			},
			refreshCookie: &http.Cookie{
				Name:     refreshCookie,
				Value:    catchError(CreateToken(ctx3, 0)),
				Path:     "/",
				Expires:  time.Now(),
				HttpOnly: true,
			},
			expectedStatusCode: http.StatusUnauthorized,
		},
		{
			name:     "Bad access-cookie",
			username: "",
			accessCookie: &http.Cookie{
				Name: accessCookie,
				Value: "BADJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJhdXRob3JpemVkIjp0cnVlLCJleHAiOjE2MzUxOTE2MTcsIn" +
					"VzZXJuYW1lIjoidXNlcjIifQ.Q7r0JDPrYtTumh1qC-Y01boK3zoJ24bWhCznfew46uQ",
				Path:     "/",
				Expires:  jwt.TimeFunc().Add(accessDuration),
				HttpOnly: true,
			},
			refreshCookie: &http.Cookie{
				Name:     refreshCookie,
				Value:    catchError(CreateToken(ctx4, refreshDuration)),
				Path:     "/",
				Expires:  jwt.TimeFunc().Add(refreshDuration),
				HttpOnly: true,
			},
			expectedStatusCode: http.StatusBadRequest,
		},
		{
			name:     "Bad refresh-cookie and expired access-cookie",
			username: "",
			accessCookie: &http.Cookie{
				Name:     accessCookie,
				Value:    catchError(CreateToken(ctx5, 0)),
				Path:     "/",
				Expires:  time.Now(),
				HttpOnly: true,
			},
			refreshCookie: &http.Cookie{
				Name: refreshCookie,
				Value: "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJhdXRob3JpemVkIjp0cnVlLCJleHAiOjE2MzUxOTUzOTYsI" +
					"nVzZXJuYW1lIjoidXNlcjMifQ.x6r3UEFopCDBADetOLB9RtRAOxn4xnL1fkx-yvO8nnM",
				Path:     "/",
				Expires:  jwt.TimeFunc().Add(refreshDuration),
				HttpOnly: true,
			},
			expectedStatusCode: http.StatusBadRequest,
		},
	}

	for _, tc := range testTable {
		t.Run(tc.name, func(t *testing.T) {
			res := httptest.NewRecorder()
			req, err := http.NewRequest("", "", nil)
			if err != nil {
				zap.L().Fatal("Failed to create request", zap.Error(err))
			}

			if !time.Now().After(tc.accessCookie.Expires) {
				req.AddCookie(tc.accessCookie)
			}

			if !time.Now().After(tc.refreshCookie.Expires) {
				req.AddCookie(tc.refreshCookie)
			}

			ValidateCookies(handlerToTest).ServeHTTP(res, req)

			assert.Equal(t, tc.expectedStatusCode, res.Code)
		})
	}
}
