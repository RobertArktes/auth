package cookies

import (
	"context"
	"net/http"
	"time"

	"github.com/golang-jwt/jwt"
	"go.uber.org/zap"
)

const (
	accessDuration  = time.Minute
	refreshDuration = time.Hour

	accessCookie  = "access-token"
	refreshCookie = "refresh-token"
)

func SendCookies(w http.ResponseWriter, ctx context.Context) error {
	accessToken, err := CreateToken(ctx, accessDuration)
	if err != nil {
		zap.L().Error("Failed to create access token", zap.Error(err))

		return err
	}

	refreshToken, err := CreateToken(ctx, refreshDuration)
	if err != nil {
		zap.L().Error("Failed to create refresh token", zap.Error(err))

		return err
	}

	accessCookie := &http.Cookie{
		Name:     accessCookie,
		Value:    accessToken,
		Path:     "/",
		Expires:  jwt.TimeFunc().Add(accessDuration),
		HttpOnly: true,
	}
	refreshCookie := &http.Cookie{
		Name:     refreshCookie,
		Value:    refreshToken,
		Path:     "/",
		Expires:  jwt.TimeFunc().Add(refreshDuration),
		HttpOnly: true,
	}

	http.SetCookie(w, accessCookie)

	http.SetCookie(w, refreshCookie)

	return nil

}

func SendEmptyCookies(w http.ResponseWriter) {
	accessCookie := &http.Cookie{
		Name:     accessCookie,
		Value:    "",
		Path:     "/",
		Expires:  time.Now(),
		HttpOnly: true,
	}
	refreshCookie := &http.Cookie{
		Name:     refreshCookie,
		Value:    "",
		Path:     "/",
		Expires:  time.Now(),
		HttpOnly: true,
	}

	http.SetCookie(w, accessCookie)

	http.SetCookie(w, refreshCookie)
}
