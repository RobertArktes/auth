package cookies

import (
	"context"

	"gitlab.com/RobertArktes/auth/internal/models"
)

type contextKey string

const (
	UsernameKey contextKey = "user"
)

func GetUser(ctx context.Context) (models.User, bool) {
	user, ok := ctx.Value(UsernameKey).(models.User)

	return user, ok
}
