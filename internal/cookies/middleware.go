package cookies

import (
	"context"
	"errors"
	"fmt"
	"net/http"

	"github.com/golang-jwt/jwt"
	"gitlab.com/RobertArktes/auth/internal/models"
	"go.uber.org/zap"
)

const (
	badTokenError = "bad token"
)

func ValidateCookies(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		if ctx, err := validateToken(r, accessCookie); err == nil {
			next.ServeHTTP(w, r.WithContext(ctx))

			return
		} else if err.Error() == badTokenError {
			zap.L().Error("Failed to validate access token", zap.Error(err))
			w.WriteHeader(http.StatusBadRequest)
			w.Write([]byte("Failed to validate token"))
		}

		ctx, err := validateToken(r, refreshCookie)
		if err != nil {
			if err.Error() == badTokenError {
				zap.L().Error("Failed to validate refresh token", zap.Error(err))
				w.WriteHeader(http.StatusBadRequest)
				w.Write([]byte("Failed to validate token"))

				return
			}
			zap.L().Error("Failed to validate refresh token", zap.Error(err))
			w.WriteHeader(http.StatusUnauthorized)
			w.Write([]byte("Failed to validate token"))

			return
		}

		if err = sendFreshCookies(w, ctx); err != nil {
			zap.L().Error("Failed to send fresh tokens", zap.Error(err))
			w.WriteHeader(http.StatusInternalServerError)
			w.Write([]byte("Failed to validate token"))

			return
		}

		next.ServeHTTP(w, r.WithContext(ctx))
	})
}

func validateToken(r *http.Request, token string) (context.Context, error) {
	cookie, err := r.Cookie(token)
	if err != nil {
		return nil, err
	}

	claims, err := getClaimsFromToken(cookie.Value)
	if err != nil {
		return nil, err
	}

	User := models.User{
		Username: claims["username"].(string)}

	return context.WithValue(
		r.Context(),
		UsernameKey,
		User,
	), nil

}

func getClaimsFromToken(token string) (jwt.MapClaims, error) {
	var claims jwt.MapClaims
	if _, err := jwt.ParseWithClaims(token, &claims, func(token *jwt.Token) (interface{}, error) {
		if _, ok := token.Method.(*jwt.SigningMethodHMAC); !ok {
			return nil, fmt.Errorf("unexpected signing method: %v", token.Header["alg"])
		}

		return []byte(jwtKey), nil
	}); err != nil {
		return nil, errors.New(badTokenError)
	}

	return claims, nil
}

func sendFreshCookies(w http.ResponseWriter, ctx context.Context) error {
	SendEmptyCookies(w)

	if err := SendCookies(w, ctx); err != nil {
		return err
	}

	return nil
}

func GetTokenClaims(token string) (jwt.MapClaims, error) {
	var claims jwt.MapClaims
	_, err := jwt.ParseWithClaims(token, &claims, func(token *jwt.Token) (interface{}, error) {
		if _, ok := token.Method.(*jwt.SigningMethodHMAC); !ok {
			return nil, fmt.Errorf("unexpected signing method: %v", token.Header["alg"])
		}

		return []byte(jwtKey), nil
	})

	if err != nil && err.Error() != "Token is expired" {
		return nil, errors.New(badTokenError)
	}

	return claims, nil
}
