package cookies

import (
	"context"
	"net/http/httptest"
	"testing"

	"github.com/stretchr/testify/assert"
)

func Test_SendCookies(t *testing.T) {
	testCases := []struct {
		name               string
		ctx                context.Context
		expectedStatusCode int
	}{
		{
			name:               "correct variant",
			expectedStatusCode: 200,
		},
	}

	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			w := httptest.NewRecorder()

			assert.Equal(t, tc.expectedStatusCode, w.Code)
		})
	}
}
