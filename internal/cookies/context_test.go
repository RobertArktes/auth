package cookies

import (
	"context"
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/RobertArktes/auth/internal/models"
)

func Test_GetUser(t *testing.T) {
	User1 := models.User{
		Username: "username",
	}
	User2 := models.User{}

	ctx := context.Background()

	testCases := []struct {
		name   string
		ctx    context.Context
		ok     bool
		result string
	}{
		{
			name:   "correct variant",
			ctx:    context.WithValue(ctx, UsernameKey, User1),
			ok:     true,
			result: "username",
		},
		{
			name: "empty username",
			ctx:  context.WithValue(ctx, UsernameKey, User2),
			ok:   true,
		},
		{
			name: "empty context",
			ctx:  ctx,
			ok:   false,
		},
	}

	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			result, ok := GetUser(tc.ctx)

			assert.Equal(t, tc.ok, ok)
			assert.Equal(t, result, result)
		})
	}
}
