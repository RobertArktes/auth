package cookies

import (
	"context"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
	"gitlab.com/RobertArktes/auth/internal/models"
)

func Test_createToken(t *testing.T) {
	User1 := models.User{
		Username: "username",
	}
	User2 := models.User{}

	ctx := context.Background()

	testCases := []struct {
		name     string
		ctx      context.Context
		duration time.Duration
	}{
		{
			name:     "correct variant",
			ctx:      context.WithValue(ctx, UsernameKey, User1),
			duration: time.Hour,
		},
		{
			name:     "wrong context",
			ctx:      context.WithValue(ctx, UsernameKey, User2),
			duration: time.Hour,
		},
	}

	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			result, _ := CreateToken(tc.ctx, tc.duration)

			assert.NotEmpty(t, result)
		})
	}
}
