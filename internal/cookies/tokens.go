package cookies

import (
	"context"
	"errors"
	"time"

	"github.com/golang-jwt/jwt"
	"go.uber.org/zap"
)

const (
	jwtKey = "jsaE69U1nTeHgs8AQr6cHoA3c1v2flC6"
)

func CreateToken(ctx context.Context, duration time.Duration) (string, error) {
	user, ok := GetUser(ctx)
	if !ok {
		zap.L().Error("Failed to get user to create token")
		err := errors.New("failed to get user from context")
		return "", err
	}

	atClaims := jwt.MapClaims{}
	atClaims["username"] = user.Username
	atClaims["authorized"] = true
	atClaims["exp"] = jwt.TimeFunc().Add(duration).Unix()

	token := jwt.NewWithClaims(jwt.SigningMethodHS256, atClaims)

	return token.SignedString([]byte(jwtKey))

}
