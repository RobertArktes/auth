package handler

import (
	"golang.org/x/crypto/bcrypt"
)

func (h *Handler) validateUser(username string, password string) error {
	password = generateHash(password)

	return h.authstorage.CheckUserExist(username, password)
}

func generateHash(pswrd string) string {
	password := []byte(pswrd)
	hashedPassword, err := bcrypt.GenerateFromPassword(password, bcrypt.DefaultCost)
	if err != nil {
		panic(err)
	}

	return string(hashedPassword)
}
