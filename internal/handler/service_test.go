package handler

import (
	"context"
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/RobertArktes/auth/internal/cookies"
	"gitlab.com/RobertArktes/auth/internal/models"
	"go.uber.org/zap"
)

func Test_HandleMe(t *testing.T) {
	User1 := models.User{
		Username: "username",
	}

	ctx := context.Background()
	ctx = context.WithValue(ctx, cookies.UsernameKey, User1)

	testCases := []struct {
		name               string
		expectedStatusCode int
		ctx                context.Context
	}{
		{
			name:               "correct variant",
			expectedStatusCode: 200,
			ctx:                context.WithValue(ctx, cookies.UsernameKey, User1),
		},
		{
			name:               "no context",
			expectedStatusCode: 500,
			ctx:                context.Background(),
		},
	}

	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			res := httptest.NewRecorder()
			req, err := http.NewRequest("", "", nil)
			if err != nil {
				zap.L().Fatal("Failed to create request", zap.Error(err))
			}

			req = req.WithContext(tc.ctx)

			HandleMe(res, req)

			assert.Equal(t, tc.expectedStatusCode, res.Code)
		})
	}
}
