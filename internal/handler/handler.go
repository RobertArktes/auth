package handler

import (
	"github.com/go-chi/chi/v5"
	httpSwagger "github.com/swaggo/http-swagger"
	_ "gitlab.com/RobertArktes/auth/docs"
	"gitlab.com/RobertArktes/auth/internal/cookies"
	"gitlab.com/RobertArktes/auth/internal/infrastructure/mongo"
)

type Handler struct {
	authstorage *mongo.Repository
}

func NewHandler(auth *mongo.Repository) *Handler {
	return &Handler{
		authstorage: auth,
	}
}

func (h *Handler) InitRoutes() *chi.Mux {
	router := chi.NewRouter()

	router.Route("/auth", func(r chi.Router) {
		r.Get("/swagger/*", httpSwagger.Handler(
			httpSwagger.URL("http://localhost:8080/auth/swagger/doc.json"),
		))

		r.Post("/login", h.Login)
		r.Post("/logout", h.Logout)

		r.Group(func(r chi.Router) {
			r.Use(cookies.ValidateCookies)
			r.Get("/me", HandleMe)
		})
	})

	return router
}
