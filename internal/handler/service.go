package handler

import (
	"encoding/json"
	"net/http"

	"gitlab.com/RobertArktes/auth/internal/cookies"
	"go.uber.org/zap"
)

//@Summary handleMe
//@Tags Endpoints
//@Security ApiKeyAuth
//@Description endpoint Me
//@ID handleme
//@Accept json
//@Produce json
//@Failure 400,404 {string} string "bad request"
//@Failure 500 {string} string "server error"
//@Failure default {string} string "other problem"
//@Router /auth/me [get]
func HandleMe(w http.ResponseWriter, r *http.Request) {
	user, ok := cookies.GetUser(r.Context())
	if !ok {
		zap.L().Error("Failed to validate user in /me")
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte("Failed to get user"))

		return
	}

	w.Header().Set("Content-Type", "application/json")

	if err := json.NewEncoder(w).Encode(user); err != nil {
		zap.L().Error("Failed to encode user", zap.Error(err))
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte("Failed to get user"))

		return
	}

	w.WriteHeader(http.StatusOK)
}
