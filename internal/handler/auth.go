package handler

import (
	"context"
	"net/http"

	"gitlab.com/RobertArktes/auth/internal/cookies"
	"gitlab.com/RobertArktes/auth/internal/models"
	"go.uber.org/zap"
)

//@Summary Login
//@Tags auth
//@Description login
//@ID login
//@Accept json
//@Produce json
//@Success 200 {string} string "token"
//@Failure 400,404 {string} string "bad request"
//@Failure 500 {string} string "server error"
//@Failure default {string} string "other problem"
//@Router /auth/login [post]
func (h *Handler) Login(w http.ResponseWriter, r *http.Request) {
	username, password, ok := r.BasicAuth()
	if !ok {
		zap.L().Error("Failed to authorized user")
		w.Header().Set("WWW-Authenticate", `Basic realm="restricted", charset="UTF-8`)
		w.WriteHeader(http.StatusUnauthorized)
		w.Write([]byte("Failed to authorized user"))

		return
	}

	User := models.User{
		Username: username,
	}

	ctx := r.Context()
	ctx = context.WithValue(ctx, cookies.UsernameKey, User)

	if err := h.validateUser(username, password); err != nil {
		zap.L().Error("Failed to validate user", zap.Error(err))
		w.Header().Set("WWW-Authenticate", `Basic realm="restricted", charset="UTF-8`)
		w.WriteHeader(http.StatusUnauthorized)
		w.Write([]byte("Failed to authorized user"))

		return
	}

	if err := cookies.SendCookies(w, ctx); err != nil {
		zap.L().Error("Failed to send cookies", zap.Error(err))
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte("Failed to authorized user"))

		return
	}

	w.WriteHeader(http.StatusOK)
}

//@Summary Logout
//@Tags auth
//@Description logout
//@ID logout
//@Accept json
//@Produce json
//@Success 200 {string} string "empty token"
//@Failure 400,404 {string} string "bad request"
//@Failure 500 {string} string "server error"
//@Failure default {string} string "other problem"
//@Router /auth/logout [post]
func (h *Handler) Logout(w http.ResponseWriter, r *http.Request) {
	cookies.SendEmptyCookies(w)

	w.WriteHeader(http.StatusOK)
}
