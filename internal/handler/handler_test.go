package handler

import (
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/go-chi/chi/v5"
	"github.com/golang/mock/gomock"
	"github.com/stretchr/testify/assert"
	"gitlab.com/RobertArktes/auth/internal/infrastructure/mongo"
	mock_mongo "gitlab.com/RobertArktes/auth/internal/infrastructure/mongo/mocks"
	"go.uber.org/zap"
)

func TestHandler_login(t *testing.T) {
	type mockBehavior func(m *mock_mongo.MockAuthorization, username string, password string)

	testCases := []struct {
		name               string
		basicAuth          string
		mockBehavior       mockBehavior
		username           string
		password           string
		expectedStatusCode int
	}{
		{
			name:      "correct variant",
			basicAuth: "Basic QWxhZGRpbjpvcGVuIHNlc2FtZQ==",
			username:  "Aladdin",
			password:  "open sesame",
			mockBehavior: func(m *mock_mongo.MockAuthorization, username string, password string) {
				m.EXPECT().CheckUserExist(username, gomock.Any()).Return(nil)
			},
			expectedStatusCode: 200,
		},
		{
			name:               "no basic auth",
			basicAuth:          "",
			mockBehavior:       func(m *mock_mongo.MockAuthorization, username string, password string) {},
			username:           "",
			password:           "",
			expectedStatusCode: 401,
		},
		{
			name:      "correct variant",
			basicAuth: "Basic QWxhZGRpbjpvcGVuIHNlc2FtZQ==",
			username:  "Aladdin",
			password:  "open sesame",
			mockBehavior: func(m *mock_mongo.MockAuthorization, username string, password string) {
				m.EXPECT().CheckUserExist(username, gomock.Any()).Return(nil)
			},
			expectedStatusCode: 200,
		},
	}

	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			c := gomock.NewController(t)
			defer c.Finish()

			auth := mock_mongo.NewMockAuthorization(c)
			tc.mockBehavior(auth, tc.username, tc.password)

			authstorage := &mongo.Repository{Authorization: auth}
			handler := NewHandler(authstorage)

			r := chi.NewRouter()
			r.Post("/login", handler.Login)

			res := httptest.NewRecorder()
			req, err := http.NewRequest("POST", "/login", nil)
			if err != nil {
				zap.L().Error("Failed to create request", zap.Error(err))
			}
			req.Header.Set("Authorization", tc.basicAuth)

			r.ServeHTTP(res, req)

			assert.Equal(t, tc.expectedStatusCode, res.Code)
		})
	}
}

func TestHandler_logout(t *testing.T) {
	testCases := []struct {
		name               string
		expectedStatusCode int
	}{
		{
			name:               "correct variant",
			expectedStatusCode: 200,
		},
	}

	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			auth := &mongo.Repository{}
			handler := NewHandler(auth)

			r := chi.NewRouter()
			r.Post("/logout", handler.Logout)

			res := httptest.NewRecorder()
			req, err := http.NewRequest("POST", "/logout", nil)
			if err != nil {
				zap.L().Error("Failed to create request", zap.Error(err))
			}

			r.ServeHTTP(res, req)

			assert.Equal(t, tc.expectedStatusCode, res.Code)
		})
	}
}

// func TestHandler_handleMe(t *testing.T) {
// 	const (
// 		accessDuration  = time.Minute
// 		refreshDuration = time.Hour
// 	)

// 	catchError := func(val string, err error) string {
// 		assert.Nil(t, err)
// 		return val
// 	}

// 	User1 := models.User{
// 		Username: "username",
// 	}

// 	ctx := context.Background()
// 	ctx = context.WithValue(ctx, cookies.UsernameKey, User1)

// 	testCases := []struct {
// 		name               string
// 		accessCookie       *http.Cookie
// 		refreshCookie      *http.Cookie
// 		expectedStatusCode int
// 	}{
// 		{
// 			name: "with cookies",
// 			accessCookie: &http.Cookie{
// 				Name:     "access-token",
// 				Value:    catchError(cookies.CreateToken(ctx, accessDuration)),
// 				Path:     "/",
// 				Expires:  jwt.TimeFunc().Add(accessDuration),
// 				HttpOnly: true,
// 			},
// 			refreshCookie: &http.Cookie{
// 				Name:     "refresh-token",
// 				Value:    catchError(cookies.CreateToken(ctx, refreshDuration)),
// 				Path:     "/",
// 				Expires:  jwt.TimeFunc().Add(refreshDuration),
// 				HttpOnly: true,
// 			},
// 			expectedStatusCode: 200,
// 		},
// 	}

// 	for _, tc := range testCases {
// 		t.Run(tc.name, func(t *testing.T) {
// 			r := chi.NewRouter()
// 			r.Get("/me", HandleMe)

// 			res := httptest.NewRecorder()
// 			req, err := http.NewRequest("GET", "/me", nil)
// 			if err != nil {
// 				zap.L().Error("Failed to create request", zap.Error(err))
// 			}

// 			r.ServeHTTP(res, req)

// 			assert.Equal(t, tc.expectedStatusCode, res.Code)
// 		})
// 	}
// }

//eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJhdXRob3JpemVkIjp0cnVlLCJleHAiOjE2NzY3MzQ0ODMsInVzZXJuYW1lIjoidXNlcm5hbWUifQ.IFAzbCuKNdFb0j1L6Z-bEYu6E8Sx93Snivbi2CMdkvE
