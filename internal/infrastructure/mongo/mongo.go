package mongo

import (
	"context"

	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"go.uber.org/zap"
)

//go:generate mockgen -source=mongo.go -destination=mocks/mock.go

const (
	authDB         = "auth"
	authCollection = "users"
)

func NewMongoDB(uri string) (*mongo.Client, error) {
	cOpts := options.Client().ApplyURI(uri)
	client, err := mongo.Connect(context.Background(), cOpts)
	if err != nil {
		zap.L().Error("Failed to get mongo client", zap.Error(err))
		return nil, err
	}

	return client, nil
}

type Authorization interface {
	CheckUserExist(username, password string) error
}

type Repository struct {
	Authorization
}

func NewRepository(db *mongo.Client) *Repository {
	return &Repository{
		Authorization: NewAuthMongo(db),
	}
}

type AuthMongo struct {
	db *mongo.Client
}

func NewAuthMongo(db *mongo.Client) *AuthMongo {
	return &AuthMongo{
		db: db,
	}
}

func (r *AuthMongo) CheckUserExist(username, password string) error {
	filter := bson.D{
		primitive.E{Key: "username", Value: username},
		primitive.E{Key: "password", Value: password},
	}

	collection := r.db.Database(authDB).Collection(authCollection)

	res := collection.FindOne(context.Background(), filter)
	if res.Err() != nil {
		return res.Err()
	}

	return nil
}
