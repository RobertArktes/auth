module gitlab.com/RobertArktes/auth

go 1.16

require (
	github.com/go-chi/chi/v5 v5.0.7
	github.com/golang-jwt/jwt v3.2.2+incompatible
	github.com/golang/mock v1.6.0
	github.com/mailru/easyjson v0.7.7 // indirect
	github.com/sirupsen/logrus v1.4.2
	github.com/spf13/viper v1.10.1
	github.com/stretchr/testify v1.7.0
	github.com/swaggo/http-swagger v1.2.0
	github.com/swaggo/swag v1.7.8
	go.mongodb.org/mongo-driver v1.8.1
	go.uber.org/atomic v1.9.0 // indirect
	go.uber.org/multierr v1.7.0 // indirect
	go.uber.org/zap v1.20.0
	golang.org/x/crypto v0.0.0-20211215153901-e495a2d5b3d3
	golang.org/x/net v0.0.0-20220127200216-cd36cc0744dd // indirect
	golang.org/x/sys v0.0.0-20220128215802-99c3d69c2c27 // indirect
	golang.org/x/tools v0.1.9 // indirect
)
